// === Sakura functions ===

var themes = [{
    name: "sakura",
    href: "https://unpkg.com/sakura.css/css/sakura.css",
}, {
    name: "sakura-earthly",
    href: "https://unpkg.com/sakura.css/css/sakura-earthly.css",
}, {
    name: "sakura-vader",
    href: "https://unpkg.com/sakura.css/css/sakura-vader.css",
}, {
    name: "sakura-dark",
    href: "https://unpkg.com/sakura.css/css/sakura-dark.css"
}, ];
var current = 0;
var sakura = document.getElementById("sakura-css");
sakura.disabled = false;

function toggleSakura() {
    if (sakura.disabled === true) {
        sakura.disabled = false
        switchTag.style.display = 'block';
    } else {
        sakura.disabled = true
        switchTag.style.display = 'none';
    }
}

var switchTag = document.getElementById("switch");
var currentThemeTag = document.getElementById("currentTheme");

function switchSakuraTheme() {
    sakura.href = themes[(current + 1) % themes.length].href;
    currentThemeTag.innerHTML = themes[(current + 1) % themes.length].name;
    current += 1
}

// === Captcha functions ===

function Captcha() {
    var alpha = new Array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    var i;
    for (i = 0; i < 6; i++) {
        var a = alpha[Math.floor(Math.random() * alpha.length)];
        var b = alpha[Math.floor(Math.random() * alpha.length)];
        var c = alpha[Math.floor(Math.random() * alpha.length)];
        var d = alpha[Math.floor(Math.random() * alpha.length)];
        var e = alpha[Math.floor(Math.random() * alpha.length)];
        var f = alpha[Math.floor(Math.random() * alpha.length)];
        var g = alpha[Math.floor(Math.random() * alpha.length)];
    }
    var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' ' + f + ' ' + g;
    document.getElementById("mainCaptcha").value = code
}

function ValidCaptcha() {
    var string1 = removeSpaces(document.getElementById('mainCaptcha').value);
    var string2 = removeSpaces(document.getElementById('txtInput').value);
    if (string1 == string2) {
        return true;
    } else {
        return false;
    }
}

function removeSpaces(string) {
    return string.split(' ').join('');
}

// Firebase function
// https://dearscottorg-default-rtdb.firebaseio.com
// https://console.firebase.google.com/project/dearscottorg/database/data

// TODO: Replace with your project's config object. You can find this
// by navigating to your project's console overview page
// (https://console.firebase.google.com/project/your-project-id/overview)
// and clicking "Add Firebase to your web app"

/*

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDs4x3LvmtfIFEFjNTe64pMgnOd4MUiqvU",
  authDomain: "dearscottorg.firebaseapp.com",
  databaseURL: "https://dearscottorg-default-rtdb.firebaseio.com",
  projectId: "dearscottorg",
  storageBucket: "dearscottorg.appspot.com",
  messagingSenderId: "132953820697",
  appId: "1:132953820697:web:38d2e9c6aa795ac207e488"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

*/

var config = {
    apiKey: "AIzaSyDs4x3LvmtfIFEFjNTe64pMgnOd4MUiqvU",
    authDomain: "dearscottorg.firebaseapp.com",
    databaseURL: "https://dearscottorg-default-rtdb.firebaseio.com",
    storageBucket: "dearscottorg.appspot.com",
  };
  
  // Initialize your Firebase app
  firebase.initializeApp(config);
  
  // Reference to your entire Firebase database
  //var myFirebase = firebase.database().ref("memories");

  // Reference to the recommendations object in your Firebase database
var memories = firebase.database().ref("memories");

// Save a new recommendation to the database using the input in the form
var submitMemory = function () {

  // Get input values from each of the form elements
  //var profile = $("#memoryProfile").val();
  var memory = $("#memory").val();
  var context = $("#context").val();
  var name = $("#name").val();

  // Push a new recommendation to the database using those values
  memories.push({
    //"profile": profile,
    "memory": memory,
    "context": context,
    "name": name
  });
};

// When the window is fully loaded, call this function.
// Note: because we are attaching an event listener to a particular HTML elemetn
// in this function, we can't do that until the HTML element in question has
// been loaded. Otherwise, we're attaching our listener to nothing, and no code
// will run when the submit button is clicked.
$(window).load(function () {

    // Find the HTML element with the id recommendationForm, and when the submit
    // event is triggered on that element, call submitRecommendation.
    $("#memoryForm").submit(submitMemory);
  
});