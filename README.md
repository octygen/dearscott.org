# DearScott.org Design Document

## Why create this?

1. **A person lives as long as they are remembered.** Our society celebrates and remembers the lives of people who "made it". Famous authors, athletes, politicians, military leaders, artists, etc.
2. **Let's celebrate all.** A site like aurelius.co remembers and celebrates the philosophy, wisdom and thinking of a particularly famous person: Emperor Marcus Aurelius - a famous stoic.

*We, at dearscott.org, believe it is extremely important to preserve the philosophy, wisdom and thinking of __all people__. That is the purpose of this site: to preserve the memories of those that were lost.*

3. **Different angles.** Each person leaves impressions on others in (i) a multitude of geographies at (ii) different times in their lives. Therefore, to achieve a comprehensive understanding of a person's spirit, one would need to hear from all the people the person has affected from around the world throughout their life. 

*We, at dearscott.org, want to gain insights into the person that was lost. Crowdsourcing of memories from everyone the person impacted allows for the level of comprehensiveness we're looking for. Crowd-sourcing is the way we want to enable the purpose of this site.*

## What?

A no-frills website that allows the family\friends (Creator and Contributor users) of an Individual to crowdsource Memories about that Individual into a Profile. The Profile is then accessible to anyone interested in seeing a random memory of that person ad-hoc.

## Who?

**Creator** - Creates and\or Manages a Profile when someone (called "the Individual" below) passes away.

* Knows unique link to the "Browse Memories" page for a Profile since s\he gets it upon Profile creation. The Creator uses this link to manages Memories for a given Profile and can distribute it to other people that s/he feels can manage the Profile. These people would then also become Creators.
* Knows unique link to "Read Memory \ Write Memory" since s\he gets it upon Profile creation. The Creator can distribute this link to all Contributors and should distribute it to other Creators. Then they'll all be able to read a random Memory or write a new one.

**Contributor** - Contributes their Memories of the Individual to the Profile

* Doesn't know the unique link to "Browse Memories" unless given by a Creator (in which case a Contributor becomes a Creator).
* Receives the link to "Read Memory \ Write Memory" from a Creator.

```plantuml

left to right direction
actor Creator
actor Contributor
rectangle "dearscott.org" as site {
  usecase "Do ... as a Creator" as CreatorUsecases
  usecase "Do ... as a Contributor" as ContributorUsecases
}

Creator --> CreatorUsecases
Contributor --> ContributorUsecases

Contributor <|-- Creator: ... is also a ...

```

## Glossary

##### Profile

* A Creator creates a Profile when some some one (heck, this can even be one's beloved dog!) passes away.
* Creation of a Profile gives two unique links
    * E.g., Octavian the Creator creates a profile for his father Nicolae Petrescu that just passed away. He enters Nicolae's Full Name, Birth Date, Birth Place, Date of Passing and Place of Passing. The website generates two unique links:
        * http://dearscott.org/aslkdnalfagf08ehaiojfla is the link to  "Read Memory \ Write Memory". Octavian distributes this to Nicolae's circle of friends.
        * http://dearscott.org/adl;amflngknasdgsagagggg is the link to "Browse Memories". Octavian uses this to manage all the Memories that have been submitted. He also distributes this to Nicolae's closest friends or family members as they would now be able to use it to see\manage all submitted Memories.
* CAPTCHA is required to create Profiles to prevent spammers. **TODO: Does this require images on the site?**

##### Memory

* This is a Memory of the deceased person
* These are created from the "Read Memory \ Write Memory" page for a particular person
* Mandatory fields are Context and Description
  * Context can be Date, Location, etc. Examples are given inline.
  * Description is free-form 0-formatting text. Examples are given inline.
* Optional field is Contributor Name
* CAPTCHA is required to write new Memories to prevent spammers - **TODO: Does this require images on the site?**

<div style="page-break-after: always;"></div>

## UI Specifications

* Site must be **ALL TEXT** with no images, sounds or videos
* There are **NO ACCOUNTS** (login\passwords) just distributable unique links
* **Each link is completely unique** and is not linked to the other one except in the backend
* Site has very little navigation between pages
* Site has **0 ads**
* The links are copied and pasted by the users for distribution - in other words, the **site does 0 emailing\contacting of people**
* If a user wants to revisit a profile, they can **bookmark** the one or two unique links they were given

##### Header Contents for both pages of a Profile

* The deceased's full name
* Birth day
* Birth place
* Death day
* Death place

*All are mandatory when a Profile is created*

##### Footer Contents

* One "about the site" link to a static page that talks about the story behind the site
  * Vision of "Let's create a dearscott for him\her and send it out!".
  * Write about the death of Scott.
  * Write about the death of Nicolae Petrescu - a totally different person with a completely different background - draganicu - sister site but in Romanian.
  * Write about the inspiration from Coco - keeping someone alive by thinking about them.
  * Write about the partnership with aurelius.co. It's not just the spirit of Marcus Aurelius or celebrities that should be preserved.
  * Exclusivity. People that are not friends of the Deceased should not know the links.
    * The site works on a basis of trust - no logins\passwords. 
    * Inspiration from start.io.
* One "donate" text link - this should go to elderlywisdomcircle.org specifically - they have a donation page on networkforgood.org

<div style="page-break-after: always;"></div>

## High-level Process Flow

```plantuml

start

partition "Profile Creation" {
    :TRIGGER: Deceased passes away;
    :Creator goes to dearscott.org
    and creates Profile;
    :Site gives the Creator unique links to two pages for the Profile:

    1) **Read Memory \ Write Memory**
    e.g., dearscott.org/3948423$#$#8498242

    2) **Browse Memories**
    e.g., dearscott.org/(T)&DDE#Q#$)!_$!_$#)$;
}

partition "Distribute Profile Links" {
    :TRIGGER: Creator wants the Profile populated with Memories;
    if (Why distribute links?) then (So people can read\write memories)
        :Creator distributes ONE LINK to
        friends of the Deceased (aka the Contributors):
        1. "Read Memory \ Write Memory";
    else (So people can manage memories)
        :Creator distributes TWO LINKS to
        to the close friends and family
        of the Deceased (aka other Creators):
        1. "Read Memory \ Write Memory" link
        2. "Browse Memories" link;
    endif
}

partition "Population and Pruning of a Profile with Memories"
    :Contributors:
    1. Populate the Profile with Memories at the "Read Memory \ Write Memory" link
    2. Report any Memories as inappropriate upon reading them at the "Read Memory \ Write Memory" link
    3. Do not see Memories marked as inappropriate on the "Read Memory \ Write Memory" page;
    :Creators can do everything a Contributor can do PLUS:
    1. Can use the "Browse Memories" page to see all submitted Memories
    2. Can report a Memory as inappropriate while browsing memories
    3. Can report as appropriate if previously marked otherwise while browsing memories;

```

<div style="page-break-after: always;"></div>

## Page Wireframes

##### Create Profile (dearscott.org home page)

```plantuml
@startsalt
{+

    <b>dearscott.org 
    ...

    {
        Hello! We, at dearscott.org are sincerely sorry for your loss and
        know what it's like to lose someone. Let's keep their spirit alive! 
    }

    {
        Create Profile
    }
    
    {
        First Name: | "Enter text here"
        Last Name: | "Enter text here" 
        Birth Day: | "Enter text here" 
        Birth Place: | "Enter text here" 
        Death Day: | "Enter text here" 
        Death Place: | "Enter text here" 
        ...
        CAPTCHA <&check>
        ...
        [SUBMIT]
    }
}

@endsalt

```

##### Read Memory \ Write Memory

```plantuml
@startsalt

{+

    <b>JACK BLACK</b> 1984/09/26 Los Angeles, California, USA - 2084/02/23 London, England, UK
    ...
    {
        <b>Read Memory
        <i>Let's make better mistakes tomorrow.
        He said this every day.
        -His Daughter
        [MARK AS INAPPROPRIATE]
        ...
        ...
        ...
        ...
        ...
        ...
    }

{
    <b>Write Memory
}

    {
    The memory | * | {SI
    <i>EXAMPLES
    <i>Can be a simple quote like:
    <i>    Make better mistakes tomorrow!
    <i>Can be a conversation like:
    <i>    Me: Don't worry, it's gonna be easy
    <i>    Him: Nothing is ever easy.
    <i>Can be something that happened
    }
 
    The context | * | {SI
    <i>EXAMPLES
    <i>He said this every day
    <i>She said this when meeting her future husband
    <i>We went camping every weekend
    <i>At her son's 12th birthday party
    <i>On our first trip to Italy
    <i>Whenever he missed the train
    <i>Whenever she got angry
    }
    Who are you? (OPTIONAL) | * | "Enter your name or relation to the Deceased"
    ...
    CAPTCHA <&check>
[SUBMIT]
    }
}

@endsalt

```

##### Browse Memories

```plantuml
@startsalt

{+
    <b>JACK BLACK</b> 1984/09/26 Los Angeles, California, USA - 2084/09/26 London, England, UK
    ...

    {
        <b>Memory | <b>Context | <b>Appropriate?
        Nothing is ever easy. | He used to say this when we worked together | [Appropriate]
        
        Make better mistakes tomorrow. | He used to tell this to all his mentees | [Inappropriate]
        ... | ... | ... | ...
        ... | ... | ... | ...
        ... | ... | ... | ...
    }
}

@endsalt

```

<div style="page-break-after: always;"></div>

## Key User Stories

##### US1: As a Contributor, I can submit a new Memory by filling out the form on the "Read Memory \ Write Memory" page for a Deceased

##### US2: As a Contributor, I can read a random Memory by going to the "Read Memory \ Write Memory" page for a Deceased

* The randomness is done upon page generation on refresh

##### US3: As a Contributor, I can prune\remove inappropriate Memories on-read to preserve an accurate image of the Deceased

* Pruning happens on the "Read Memory \ Write Memory" page by clicking "Mark as Inappropriate" to report a Memory
* This happens when a submitted Memory is viewed in any way as disrespectful to the Deceased by a Contributor
* Therefore pruning out inappropriate Memories is "crowd-sourced" by all Contributors reading random Memories
* Marking as inappropriate does not delete the Memory but just marks it in the DB to not be displayed
* There is no "upvoting" of Memories

##### US4: As a Creator, I can manage the list of submitted Memories in list-form by marking them as inappropriate or marking them as appropriate

* Pruning happens on the "Browse Memories" page by marking a line item as inappropriate. This happens by clicking the "Inappropriate" button. Note that this page displays all memories in a Profile.  
* A Creator with this link can also re-enable a previously pruned Memory by clicking "Appropriate" if a Memory was previously marked otherwise.
* The list should display Memories marked as inappropriate in gray.
* Submitted Memories cannot be edited or deleted via the UI.

<div style="page-break-after: always;"></div>

## FAQ

##### QUESTION: How is spamming prevented at the profile creation or memory writing levels?

* Text-only CAPTCHA.

##### RISK: Site could prevent the survivors of the deceased from getting over the death of a loved one in a speedy way.

* Expand scope for site to individuals that are alive that have had an impact on you (e.g., celebrate anniversaries, celebrate their spirit while they are still alive).
* E.g., "Glenn is turning 60. Let's celebrate by you submitting memories and how he had an impact on you!".

##### RISK: People can submit inappropriate memories. How to prevent this?

* The definition of "inappropriate" is subjective and should be up to the Creators and the Contributors.
* When reading memories, Contributors can mark a memory as inappropriate.
* Creators can mark memories as appropriate or inappropriate while Browsing Memories.

##### QUESTION: Who pays for hosting if this site does scale and gets users?

* Octavian Petrescu. If he runs out of $$$, we arrange for a donation link. Until that scenario, the donation link should go to the Elderly Wisdom Circle.

##### RISK: If they don’t bookmark the URLs, without a login, how would they ever get back to the Profile?

* Heavily reinforce the saving of the two links upon Profile creation. Alternatively, heavily reinforce the "Share these links right away!" principle. Once the links are shared, they can be retrieved.
* If the above somehow fails and the links are lost before distribution, the Creator can create another Profile. There will be duplicates but these can be mitigated. 
* At the end of every day, months OR year, dearscott.org can delete empty Profiles via, for example, a cron job.